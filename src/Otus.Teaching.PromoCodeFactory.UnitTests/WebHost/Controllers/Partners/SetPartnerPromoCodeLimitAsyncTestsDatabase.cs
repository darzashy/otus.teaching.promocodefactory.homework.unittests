﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoFixture;
using AutoFixture.Xunit2;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners.Fixtures;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTestsDatabase : IClassFixture<InMemoryTestFixture>
    {
        private readonly InMemoryTestFixture _fixture;

        public SetPartnerPromoCodeLimitAsyncTestsDatabase(InMemoryTestFixture fixture)
        {
            _fixture = fixture ?? throw new ArgumentNullException(nameof(fixture));
        }
        
        [Theory, AutoData]
        public async void NewPartnerLimit_IsSavedToDatabase(
            SetPartnerPromoCodeLimitRequest request)
        {
            //Arrange
            request.Limit = 5;
            var partner = _fixture.Fixture.Build<Partner>()
                .With(p => p.IsActive, true)
                .With(p => p.PartnerLimits, new List<PartnerPromoCodeLimit>())
                .Create();
            var partnerId = partner.Id;

            //Act
            await _fixture.PartnersRepositoryDb.AddAsync(partner);
            var result = await _fixture.PartnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeOfType<CreatedAtRouteResult>();
            var partnerInDb = await _fixture.PartnersRepositoryDb.GetByIdAsync(partnerId);
            partnerInDb.PartnerLimits.Count.Should().Be(1);
            partnerInDb.PartnerLimits.First().Limit.Should().Be(request.Limit);
            partnerInDb.PartnerLimits.First().EndDate.Should().Be(request.EndDate);
        }
    }
}