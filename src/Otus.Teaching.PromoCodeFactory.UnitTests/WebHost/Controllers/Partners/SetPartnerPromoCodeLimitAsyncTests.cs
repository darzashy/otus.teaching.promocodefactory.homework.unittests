﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoFixture;
using AutoFixture.Xunit2;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners.Fixtures;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests : IClassFixture<TestFixture>
    {
        private readonly TestFixture _fixture;

        public SetPartnerPromoCodeLimitAsyncTests(TestFixture fixture)
        {
            _fixture = fixture ?? throw new ArgumentNullException(nameof(fixture));
        }

        /// <summary>
        /// 1. Если партнер не найден, то нужно выдать ошибку 404;
        /// </summary>
        [Theory, AutoData]
        public async void Partner_IsNotFound_NotFoundResult(
            Guid partnerId, SetPartnerPromoCodeLimitRequest request)
        {
            //Arrange
            _fixture.PartnersRepositoryMock.Setup(repository => 
                repository.GetByIdAsync(partnerId)).ReturnsAsync((Partner)null);

            //Act
            var result = await _fixture.PartnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            //Assert
            result.Should().BeOfType<NotFoundResult>();
        }

        /// <summary>
        /// 2. Если партнер заблокирован, то есть поле IsActive=false в классе Partner, то также нужно выдать ошибку 400;
        /// </summary>
        [Theory, AutoData]
        public async void Partner_IsNotActive_BadRequestResult(
            Guid partnerId, SetPartnerPromoCodeLimitRequest request)
        {
            //Arrange
            var partner = _fixture.Fixture.Build<Partner>()
                .With(p => p.IsActive, false)
                .Without(p => p.PartnerLimits)
                .Create();
            _fixture.PartnersRepositoryMock.Setup(repository => 
                repository.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            //Act
            var result = await _fixture.PartnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            //Assert
            result.Should().BeOfType<BadRequestObjectResult>().Which.Value.Should().Be(PartnersController.PartnerIsNotActive);
        }

        /// <summary>
        /// 3.1 Если партнеру с активным лимитом выставляется новый лимит,
        /// то мы должны обнулить количество промокодов, которые партнер выдал NumberIssuedPromoCodes
        /// </summary>
        [Theory, AutoData]
        public async void NumberIssuedPromoCodes_SetLimitToPartnerWithActiveLimit_Zero(
            Guid partnerId, SetPartnerPromoCodeLimitRequest request)
        {
            //Arrange
            request.Limit = 5;
            var partner = _fixture.Fixture.Build<Partner>()
                .With(p => p.IsActive, true)
                .With(x => x.NumberIssuedPromoCodes, 5)
                .Without(p => p.PartnerLimits)
                .Create();
            var partnerLimits = _fixture.Fixture.Build<PartnerPromoCodeLimit>()
                .With(x => x.Partner, partner)
                .With(x => x.CancelDate, (DateTime?)null)
                .CreateMany(1)
                .ToList();
            partner.PartnerLimits = partnerLimits;
            _fixture.PartnersRepositoryMock.Setup(repository => 
                repository.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            //Act
            await _fixture.PartnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);
            
            //Assert
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }
        
        /// <summary>
        /// 3.2 Если партнеру выставляется лимит, и предыдущий лимит закончился, то количество не обнуляется;
        /// </summary>
        [Theory, AutoData]
        public async void NumberIssuedPromoCodes_SetLimitToPartnerWithoutActiveLimit_NotChanged(
            Guid partnerId, SetPartnerPromoCodeLimitRequest request)
        {
            //Arrange
            request.Limit = 5;
            var numberIssuedPromoCodes = _fixture.Fixture.Create<int>();
            var partner = _fixture.Fixture.Build<Partner>()
                .With(p => p.IsActive, true)
                .With(x => x.NumberIssuedPromoCodes, numberIssuedPromoCodes)
                .With(p => p.PartnerLimits, new List<PartnerPromoCodeLimit>())
                .Create();
            var partnerLimits = _fixture.Fixture.Build<PartnerPromoCodeLimit>()
                .With(x => x.Partner, partner)
                .With(x => x.CancelDate, DateTime.Today)
                .CreateMany(1)
                .ToList();
            partner.PartnerLimits = partnerLimits;
            _fixture.PartnersRepositoryMock.Setup(repository => 
                repository.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            //Act
            await _fixture.PartnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);
            
            //Assert
            partner.NumberIssuedPromoCodes.Should().Be(numberIssuedPromoCodes);
        }
        
        /// <summary>
        /// 4. При установке лимита нужно отключить предыдущий лимит;
        /// </summary>
        /// <param name="partnerId"></param>
        /// <param name="request"></param>
        [Theory, AutoData]
        public async void PreviousLimit_SetLimit_IsCancelled(
            Guid partnerId, SetPartnerPromoCodeLimitRequest request)
        {
            //Arrange
            request.Limit = 5;
            var startDate = DateTime.Now;
            var partner = _fixture.Fixture.Build<Partner>()
                .With(p => p.IsActive, true)
                .With(x => x.NumberIssuedPromoCodes, 5)
                .Without(p => p.PartnerLimits)
                .Create();
            var partnerLimit = _fixture.Fixture.Build<PartnerPromoCodeLimit>()
                .With(x => x.Partner, partner)
                .With(x => x.CancelDate, (DateTime?)null)
                .Create();
            partner.PartnerLimits = new List<PartnerPromoCodeLimit> { partnerLimit };
            _fixture.PartnersRepositoryMock.Setup(repository => 
                repository.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            //Act
            await _fixture.PartnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);
            
            //Assert
            partnerLimit.CancelDate?.Should().BeAfter(startDate);
            partnerLimit.CancelDate?.Should().BeBefore(DateTime.Now);
        }

        /// <summary>
        /// 5. Лимит должен быть больше 0
        /// </summary>
        [Theory, AutoData]
        public async void SetLimitNotAboveZero_BadRequest(
            Guid partnerId, SetPartnerPromoCodeLimitRequest request)
        {
            //Arrange
            request.Limit = -3;
            var partner = _fixture.Fixture.Build<Partner>()
                .With(p => p.IsActive, true)
                .With(p => p.PartnerLimits, new List<PartnerPromoCodeLimit>())
                .Create();
            _fixture.PartnersRepositoryMock.Setup(repository => 
                repository.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            //Act
            var result = await _fixture.PartnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            //Assert
            result.Should().BeOfType<BadRequestObjectResult>()
                .Which.Value.Should().Be(PartnersController.LimitMustBeAboveZero);
        }
    }
}