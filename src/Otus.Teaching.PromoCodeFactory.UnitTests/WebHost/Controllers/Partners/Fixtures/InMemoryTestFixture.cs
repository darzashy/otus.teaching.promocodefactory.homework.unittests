﻿using System;
using AutoFixture;
using AutoFixture.AutoMoq;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Helpers;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners.Fixtures
{
    public class InMemoryTestFixture : IDisposable
    {
        public PartnersController PartnersController { get; }
        public IFixture Fixture { get; }
        public IRepository<Partner> PartnersRepositoryDb { get; }
        
        public InMemoryTestFixture()
        {
            var serviceProvider = InMemoryConfiguration.GetServiceProvider();
            PartnersRepositoryDb = serviceProvider.GetService<IRepository<Partner>>();
            Fixture = new Fixture().Customize(new AutoMoqCustomization());
            Fixture.Inject(PartnersRepositoryDb);
            PartnersController = Fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }
        
        public void Dispose()
        {
        }
    }
}