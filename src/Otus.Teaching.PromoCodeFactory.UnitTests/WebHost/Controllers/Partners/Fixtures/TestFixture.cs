﻿using System;
using AutoFixture;
using AutoFixture.AutoMoq;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners.Fixtures
{
    public class TestFixture : IDisposable
    {
        public Mock<IRepository<Partner>> PartnersRepositoryMock { get; }
        public PartnersController PartnersController { get; }
        public IFixture Fixture { get; }
        
        public TestFixture()
        {
            Fixture = new Fixture().Customize(new AutoMoqCustomization());
            PartnersRepositoryMock = Fixture.Freeze<Mock<IRepository<Partner>>>();
            PartnersController = Fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }
        
        public void Dispose()
        {
        }
    }
}